﻿using System;
using System.Threading.Tasks;
using timesheet.data.Repository;

namespace timesheet.data.UnitOfWork
{
    public interface IUnitOfWork  :  IDisposable
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;
        Task<int> Commit();
        void Rollback();
    }
}
