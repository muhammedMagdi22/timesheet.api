﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.data.Repository;

namespace timesheet.data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public TimesheetDb _dataContext;
        private readonly Dictionary<string, dynamic> _repositories;

        public UnitOfWork(TimesheetDb dataContext)
        {
            _dataContext = dataContext;
            _repositories = new Dictionary<string, dynamic>();
        }

        public async Task<int> Commit()
        {
            return await _dataContext.SaveChangesAsync();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            return new Repository<TEntity>(_dataContext);
        }


        public void Rollback()
        {
            _dataContext.ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }
    }
}
