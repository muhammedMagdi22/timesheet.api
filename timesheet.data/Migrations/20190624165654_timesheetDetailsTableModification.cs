﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class timesheetDetailsTableModification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Friday",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "Monday",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "Saturday",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "Sunday",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "Thursday",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "Tuesday",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "Wednesday",
                table: "TimeSheetDetails");

            migrationBuilder.AddColumn<double>(
                name: "EffortTime",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "WorkDate",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EffortTime",
                table: "TimeSheetDetails");

            migrationBuilder.DropColumn(
                name: "WorkDate",
                table: "TimeSheetDetails");

            migrationBuilder.AddColumn<int>(
                name: "Friday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Monday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Saturday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Sunday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Thursday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Tuesday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Wednesday",
                table: "TimeSheetDetails",
                nullable: false,
                defaultValue: 0);
        }
    }
}
