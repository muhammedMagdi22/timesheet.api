using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.business.Services;
using timesheet.business.mapper;
using timesheet.model;
using Xunit;
using timesheet.data.Repository;

namespace timesheet.tests
{
    public class EmployeeServiceTests
    {
        private Mock<IRepository<Employee>> _employeeRepo;

        public EmployeeServiceTests()
        {
            _employeeRepo = new Mock<IRepository<Employee>>();

        }

        private EmployeeService Service()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            return new EmployeeService(_employeeRepo.Object, mapper );
        }

        [Fact]
        public void CurrentWeekTotalEffort_Test()
        {

            List<TimeSheetDetails> emp1_timsheetList = new List<TimeSheetDetails>()
            {
                new TimeSheetDetails() { Id = 1, EmployeeId = 1, TaskId = 1, WorkDate = DateTime.Now, EffortTime = 8 },
                new TimeSheetDetails() { Id = 2, EmployeeId = 1, TaskId = 2, WorkDate = DateTime.Now, EffortTime = 8 },
            };

            List<TimeSheetDetails> emp2_timsheetList = new List<TimeSheetDetails>()
            {
                new TimeSheetDetails() { Id = 3, EmployeeId = 2, TaskId = 2, WorkDate = DateTime.Now, EffortTime = 8 }
            };

            List<Employee> employeeList = new List<Employee>()
            {
                new Employee(){ Id=1, Code="RA", Name="Ali", TimesheetDetails=emp1_timsheetList},
                new Employee(){ Id=2, Code="RC", Name="Alaa", TimesheetDetails=emp2_timsheetList},
            };

            _employeeRepo.Setup(r => r.Queryable()).Returns(employeeList.AsQueryable());
            var result = Service().GetAll();
            Assert.Equal(2,result.Count);
            Assert.Equal(16, result.Where(r => r.Name == "Ali").FirstOrDefault().TotalEffort);
            Assert.Equal(2, result.Where(r => r.Name == "Ali").FirstOrDefault().AvgEffort);


            Assert.Equal(8, result.Where(r => r.Name == "Alaa").FirstOrDefault().TotalEffort);
            Assert.Equal(1, result.Where(r => r.Name == "Alaa").FirstOrDefault().AvgEffort);
        }
    }
}
