﻿using System;
using timesheet.business.helpers;
using Xunit;

namespace timesheet.tests
{
    public class DateTimeExtenstionsTests
    {
        [Fact]
        public void FirstDayInMonth_Test()
        {
            var expected = new DateTime(2019, 6, 1);
            var dt = new DateTime(2019, 6, 9);
            var actual = dt.FirstDayOfMonth();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LastDayInMonth_Test()
        {
            var expected = new DateTime(2019, 6, 30);
            var dt = new DateTime(2019, 6, 9);
            var actual = dt.LastDayOfMonth();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void firstDayOfWeek5_Test()
        {
            var expected = new DateTime(2019, 6, 29);
            var dt = new DateTime(2019, 6, 30);
            var actual = dt.FirstDayOfWeek();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void firstDayOfWeek3_Test()
        {
            var expected = new DateTime(2019, 6, 15);
            var dt = new DateTime(2019, 6, 18);
            var actual = dt.FirstDayOfWeek();
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void LastDayOfWeek2_Test()
        {
            var expected = new DateTime(2019, 6, 14);
            var dt = new DateTime(2019, 6, 11);
            var actual = dt.FirstDayOfWeek().LastDayOfWeek();
            Assert.Equal(expected, actual);
        }

    }
}
