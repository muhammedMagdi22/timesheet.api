﻿using AutoMapper;
using System;
using System.Linq;
using timesheet.business.DTO;
using timesheet.business.helpers;
using timesheet.model;

namespace timesheet.business.mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Employee, EmployeeDto>().
                ForMember(dest => dest.TotalEffort, 
                          opt => opt.MapFrom(src => src.TimesheetDetails.
                                Where(t => t.WorkDate >= DateTime.Now.FirstDayOfWeek() && t.WorkDate <= DateTime.Now.LastDayOfWeek()).Sum(t => t.EffortTime)));
            CreateMap<Task, TaskDto>();

        }
    }
}
