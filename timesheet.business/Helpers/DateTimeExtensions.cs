﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.helpers
{
   public static class DateTimeExtensions
    {
        public const DayOfWeek FirstDay = DayOfWeek.Saturday;


        public static DateTime FirstDayOfWeek(this DateTime dt)
        {
            var diff = dt.DayOfWeek - FirstDay;
            if (diff < 0)
                diff += 7;
            return dt.AddDays(-diff).Date;
        }

        public static DateTime LastDayOfWeek(this DateTime dt)
        {
            var firstDayOfWeek = dt.FirstDayOfWeek();
            return firstDayOfWeek.AddDays(6);
        }

        public static DateTime FirstDayOfMonth(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        public static DateTime LastDayOfMonth(this DateTime dt)
        {
            return dt.FirstDayOfMonth().AddMonths(1).AddDays(-1);
        }
    }
}
