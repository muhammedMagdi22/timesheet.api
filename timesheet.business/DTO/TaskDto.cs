﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.DTO
{
    public class TaskDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
