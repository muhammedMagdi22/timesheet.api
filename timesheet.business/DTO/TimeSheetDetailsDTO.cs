﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.DTO
{
    public class TimesheetDetailsDto
    {
        public string TaskName { get; set; }
        public List<TimesheetDetailItem> TimesheetDetails { get; set; }
    }
}
