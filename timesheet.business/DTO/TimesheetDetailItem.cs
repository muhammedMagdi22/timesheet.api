﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.DTO
{
    public class TimesheetDetailItem
    {
        public DateTime WorkDate { get; set; }

        public double EffortTime { get; set; }
    }
}
