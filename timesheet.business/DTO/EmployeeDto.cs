﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.DTO
{
    public class EmployeeDto
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public double TotalEffort { get; set; }

        public double AvgEffort {   get { return Math.Round(TotalEffort / 7.0); } }
    }
}
