﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using timesheet.business.DTO;
using timesheet.data;
using timesheet.data.Services;
using timesheet.model;

namespace timesheet.business.Services
{
    public interface ITimesheetDetailService : IService<TimeSheetDetails>
    {
        List<TimesheetDetailsDto> GetTimeSheetDetails(int employeeId, DateTime startDate, DateTime endDate);
        Task<int> Add(TimeSheetDetails timeSheetDetails);
    }
}