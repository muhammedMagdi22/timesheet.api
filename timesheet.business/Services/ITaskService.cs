﻿using System.Collections.Generic;
using timesheet.business.DTO;
using timesheet.data.Services;
using timesheet.model;

namespace timesheet.business.Services
{
    public interface ITaskService :IService<Task>
    {
        List<TaskDto> GetTasks();
    }
}
