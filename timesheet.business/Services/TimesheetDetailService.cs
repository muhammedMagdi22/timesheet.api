﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.business.DTO;
using timesheet.data;
using timesheet.data.Repository;
using timesheet.data.Services;
using timesheet.data.UnitOfWork;
using timesheet.model;

namespace timesheet.business.Services
{
    public class TimesheetDetailService : Service<TimeSheetDetails>, ITimesheetDetailService
    {
        private readonly IServiceProvider serviceProvider;

        public TimesheetDetailService(IRepository<TimeSheetDetails> repository, IServiceProvider serviceProvider) : base(repository)
        {
            this.serviceProvider = serviceProvider;
        }

        public override IQueryable<TimeSheetDetails> Queryable()
        {
            return base.Queryable().Include(t => t.Employee).Include(t => t.Task);
        }

        public List<TimesheetDetailsDto> GetTimeSheetDetails(int employeeId, DateTime startDate, DateTime endDate)
        {
            return Queryable()
                .Where(t => t.EmployeeId == employeeId && t.WorkDate >= startDate && t.WorkDate <= endDate)
                .GroupBy(t => t.Task.Name)
                .Select(g => new TimesheetDetailsDto()
                {
                    TaskName= g.Key,
                    TimesheetDetails = g.Select(t => new TimesheetDetailItem()
                    {
                        WorkDate = t.WorkDate,
                        EffortTime = t.EffortTime
                    }).ToList()
                }).ToList();
        }


        public async Task<int> Add(TimeSheetDetails timeSheetDetails)
        {
            int affected = 0;
            using (var unitOfWork = (IUnitOfWork)serviceProvider.GetService(typeof(IUnitOfWork)))
            {
                var repo = unitOfWork.Repository<TimeSheetDetails>();
                repo.Insert(timeSheetDetails);
                affected = await unitOfWork.Commit();
            }
            return affected;
        }
    }
}
