﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using timesheet.business.DTO;
using timesheet.data;
using timesheet.data.Repository;
using timesheet.data.Services;
using timesheet.model;

namespace timesheet.business.Services
{
    public class TaskService: Service<Task>, ITaskService
    {
        private readonly IMapper mapper;
        public TaskService(IRepository<Task> repository, IMapper mapper) : base(repository)
        {
            this.mapper = mapper;
        }

        public List<TaskDto> GetTasks()
        {
            return Queryable().Select(c => mapper.Map<TaskDto>(c)).ToList();
        }
    }
}
