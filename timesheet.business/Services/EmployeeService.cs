﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using timesheet.business.DTO;
using timesheet.data;
using timesheet.data.Repository;
using timesheet.data.Services;
using timesheet.model;

namespace timesheet.business.Services
{
    public class EmployeeService : Service<Employee>, IEmployeeService
    {
        private readonly IMapper mapper;
        public EmployeeService(IRepository<Employee> repository, IMapper mapper) : base(repository)
        {
            this.mapper = mapper;
        }

        public List<EmployeeDto> GetAll()
        {
            return Queryable().Include(e=>e.TimesheetDetails).Select(employee => mapper.Map<EmployeeDto>(employee)).ToList();
        }
    }
}
