﻿using System.Collections.Generic;
using timesheet.business.DTO;
using timesheet.data;
using timesheet.data.Services;
using timesheet.model;

namespace timesheet.business.Services
{
    public interface IEmployeeService : IService<Employee>
    {
        List<EmployeeDto> GetAll();
    }
}
