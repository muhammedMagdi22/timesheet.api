﻿using System;

namespace timesheet.business.Utility.Nlogger
{
    public interface ILogger
    {
        void Log(LogCategory logCategory, string logMessage);
        void Log(Exception exception);
    }
}
