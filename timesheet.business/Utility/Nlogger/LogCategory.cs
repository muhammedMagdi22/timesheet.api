﻿namespace timesheet.business.Utility.Nlogger
{
    public enum LogCategory
    {
        Trace, 
        Debug,
        Info, 
        Warn, 
        Error,
        Fatal,
    }
}
