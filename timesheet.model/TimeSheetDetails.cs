﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    public class TimeSheetDetails
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public DateTime WorkDate { get; set; }

        public double EffortTime { get; set; }

        [ForeignKey("Task")]
        public int TaskId { get; set; }

        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }

        public Task Task { get; set; }

        public Employee Employee { get; set; }
    }
}
