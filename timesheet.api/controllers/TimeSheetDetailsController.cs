﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business.DTO;
using timesheet.business.Services;
using timesheet.model;

namespace timesheet.api.controllers
{
    /// <summary>
    /// TimeSheetDetails APIs
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TimeSheetDetailsController : ControllerBase
    {
        private readonly ITimesheetDetailService timeSheetDetailsService;
        
        public TimeSheetDetailsController(ITimesheetDetailService timeSheetDetailsService)
        {
            this.timeSheetDetailsService = timeSheetDetailsService;
        }
        /// <summary>
        /// Get employee's Timesheet details within specific period
        /// </summary>
        /// <param name="employeeId">Employee Id to search for</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns> List of All matched timesheets</returns>
        [HttpGet("{employeeId}/{startDate}/{endDate}")]
        [ProducesResponseType(typeof(List<TimesheetDetailsDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetTimeSheetDetails([FromRoute] int employeeId, DateTime startDate, DateTime endDate)
        {
            var timeSheetDetails = timeSheetDetailsService.GetTimeSheetDetails(employeeId, startDate, endDate);
            if (timeSheetDetails == null)
            {
                return NotFound();
            }

            return Ok(timeSheetDetails);
        }


        /// <summary>
        /// Inser timesheet details record
        /// </summary>
        /// <param name="timeSheetDetails"> Timesheet details</param>
        /// <returns>timeSheetDetails</returns>
        [HttpPost]
        [ProducesResponseType(typeof(TimeSheetDetails), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostTimeSheetDetails([FromBody] TimeSheetDetails timeSheetDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await timeSheetDetailsService.Add(timeSheetDetails);
            return Ok(timeSheetDetails);
        }
    }
}