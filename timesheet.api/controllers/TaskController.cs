﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using timesheet.business.DTO;
using timesheet.business.Services;

namespace timesheet.api.controllers
{
    /// <summary>
    /// Task API
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService taskService;
        
        public TaskController(ITaskService taskService)
        {
            this.taskService = taskService;
        }

        /// <summary>
        /// Get all Tasks
        /// </summary>
        /// <returns> List of all tasks</returns>
        [HttpGet]
        public IEnumerable<TaskDto> Get()
        {
            return taskService.GetTasks();
        }
    }
}