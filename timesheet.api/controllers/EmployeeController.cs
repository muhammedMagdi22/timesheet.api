﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.business;
using timesheet.business.DTO;
using timesheet.business.Services;

namespace timesheet.api.controllers
{
    /// <summary>
    /// Employee API
    /// </summary>
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService employeeService;
        private readonly ITimesheetDetailService timeSheetDetailsService;
  
        public EmployeeController(IEmployeeService employeeService, ITimesheetDetailService timeSheetDetailsService)
        {
            this.employeeService = employeeService;
            this.timeSheetDetailsService = timeSheetDetailsService;
        }
        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <returns> List of all employees</returns>
        [HttpGet("getall")]
        [ProducesResponseType(typeof(List<EmployeeDto>), StatusCodes.Status200OK)]
        public IActionResult GetAll()
        {
            var items = this.employeeService.GetAll();
            return new ObjectResult(items);
        }

    }
}