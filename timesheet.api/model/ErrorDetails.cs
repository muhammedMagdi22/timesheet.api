﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace timesheet.api.model
{
    /// <summary>
    /// custom Error Class
    /// </summary>
    public class ErrorDetails
    {
        /// <summary>
        /// Status code
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Tostring 
        /// </summary>
        /// <returns>Json string</returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
